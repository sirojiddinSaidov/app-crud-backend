package uz.pdp.appcrudbackend.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appcrudbackend.payload.ApiResult;
import uz.pdp.appcrudbackend.payload.SignDTO;
import uz.pdp.appcrudbackend.payload.TokenDTO;
import uz.pdp.appcrudbackend.service.AuthService;

@RestController
@RequestMapping(AuthController.BASE_PATH)
@RequiredArgsConstructor
public class AuthController {

    public static final String BASE_PATH = "/api/auth";
    public static final String LOGIN_PATH = "/login";

    private final AuthService authService;

    @PostMapping(LOGIN_PATH)
    public HttpEntity<ApiResult<TokenDTO>> login(@Valid @RequestBody SignDTO signDTO) {
        ApiResult<TokenDTO> result = authService.signIn(signDTO);
        return ResponseEntity.ok(result);
    }
}
