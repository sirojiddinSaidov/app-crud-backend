package uz.pdp.appcrudbackend.service;

import jdk.jfr.Label;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.appcrudbackend.payload.ApiResult;
import uz.pdp.appcrudbackend.payload.SignDTO;
import uz.pdp.appcrudbackend.payload.TokenDTO;
import uz.pdp.appcrudbackend.repository.UserRepository;

@Service
public class AuthService implements UserDetailsService {


    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;

    public AuthService(@Lazy AuthenticationManager authenticationManager, UserRepository userRepository) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Use topilmadi: " + username));
    }

    public ApiResult<TokenDTO> signIn(SignDTO signDTO) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(signDTO.username(), signDTO.password());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        System.out.println(authentication);
        //TODO change user details child
        // generate token
        // token check in filter
        // set SecurityContextHolder
        // do authorize
        return null;
    }
}
