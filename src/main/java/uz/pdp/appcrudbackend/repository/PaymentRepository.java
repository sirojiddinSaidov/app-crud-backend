package uz.pdp.appcrudbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appcrudbackend.entity.Payment;

@RepositoryRestResource(path = "payment")
public interface PaymentRepository extends JpaRepository<Payment, Integer> {
}