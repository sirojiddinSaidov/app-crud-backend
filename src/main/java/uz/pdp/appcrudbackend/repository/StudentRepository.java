package uz.pdp.appcrudbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appcrudbackend.entity.Student;

@RepositoryRestResource(path = "student")
public interface StudentRepository extends JpaRepository<Student, Integer> {
}