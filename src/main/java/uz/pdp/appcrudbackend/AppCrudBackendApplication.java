package uz.pdp.appcrudbackend;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import uz.pdp.appcrudbackend.entity.User;
import uz.pdp.appcrudbackend.enums.RoleEnum;
import uz.pdp.appcrudbackend.repository.UserRepository;

@SpringBootApplication
@RequiredArgsConstructor
public class AppCrudBackendApplication {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(AppCrudBackendApplication.class, args);
    }


    @Bean
    public CommandLineRunner commandLineRunner() {
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                userRepository.save(User.builder()
                        .role(RoleEnum.ADMIN)
                        .password(passwordEncoder.encode("root123"))
                        .username("admin")
                        .build());
            }
        };
    }
}
