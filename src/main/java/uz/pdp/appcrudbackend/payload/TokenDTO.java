package uz.pdp.appcrudbackend.payload;


import lombok.*;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TokenDTO {

    private final String tokenType = "Bearer ";
    private String accessToken;
    private String refreshToken;
}
