package uz.pdp.appcrudbackend.payload;


import jakarta.validation.constraints.NotBlank;

public record SignDTO(@NotBlank String username, @NotBlank String password) {
}
